const express = require('express');
const app = express();
const Redis = require("ioredis")
app.use(express.json());
const _ = require('lodash');
const uniqid = require('uniqid');
const redis = new Redis()
const axios = require('axios');
const {createClient,SchemaFieldTypes} = require('redis');
const client = createClient();
const asyncForEach = require('async-await-foreach');

// app.get('/api/redis',async(req,res,next)=>{
//     await client.connect()
//     try {
//         await client.ft.create('idx:animals', {
//             name: {
//               type: SchemaFieldTypes.TEXT,
//               sortable: true
//             },
//             species: SchemaFieldTypes.TAG,
//             age: SchemaFieldTypes.NUMERIC
//           }, {
//             ON: 'HASH',
//             PREFIX: 'noderedis:animals'
//           });
//         // await Promise.all([
//         //     client.hSet('noderedis:animals:1', {name: 'Fluffy', species: 'cat', age: 3}),
//         //     client.hSet('noderedis:animals:2', {name: 'Ginger', species: 'cat', age: 4}),
//         //     client.hSet('noderedis:animals:3', {name: 'Rover', species: 'dog', age: 9}),
//         //     client.hSet('noderedis:animals:4', {name: 'Fido', species: 'dog', age: 7})
//         // ]);
        
//         const results = await client.ft.search(
//             'idx:animals', 
//             '@species:{dog}',
//             {
//             SORTBY: {
//                 BY: 'age',
//                 DIRECTION: 'DESC' // or 'ASC (default if DIRECTION is not present)
//             }
//             }
//         );
//         console.log('results: ', results);
//     } catch (error) {
//         if (error.message === 'Index already exists') {
//             console.log('Index exists already, skipped creation.');
//           } else {
//             // Something went wrong, perhaps RediSearch isn't installed...
//             console.error(error);
//             process.exit(1);
//           }
//     }
// })
app.post('/add/:session', async function(req, res){
    const {session} = req.params;
    const {sku,price,amount} = req.body;
    if(session===null || sku==null || amount==null || price==null){
        return res.send('Cart item details or session was not given');
    }
    if(amount === 0){
        return await redis.hdel('cart:'+session, _.toString(sku))
    }
    let productStore = await redis.hget('cart:'+session,_.toString(sku))
 
    if(productStore){
        productStore =  JSON.parse(productStore)
        productStore.amount += amount;
        await redis.hset('cart:'+session,_.toString(sku), JSON.stringify(productStore));
        return res.send('Thêm sản phẩm mới thành công')
    }
    await redis.hset('cart:'+session,_.toString(sku), JSON.stringify(req.body));
    await redis.expire('cart:'+session,1000)
    return res.send('Thêm sản phẩm thành công!!!')
})
app.get('/cart/:session', async (req,res) => {
    const {session} = req.params;
    const products = []
    const productList = await redis.hgetall('cart:'+session)
    if(_.isEmpty(productList)){
        return res.send('Giỏ hàng đang trống... ')
    }
    for (const product of Object.keys(productList)){
        products.push(JSON.parse(productList[product]))
    }
    return res.send(products)
})
app.delete('/delete/:session', async (req,res) => {
    await redis.hdel('cart:'+req.params.session,_.toString(req.body.sku))
    return res.send('Xoá sản phẩm trong giỏ hàng thành công')
})
app.delete('/:session', async (req,res) => {
    await redis.expire('cart:'+req.params.session,0)
    return res.send('Xoá giỏ hàng thành công')
})
app.get('/order/:session', async (req,res)=>{
    const discounts = [
        {
            id:1,
            name:'EVENT_678',
            discount: 20
        },
        {
            id:2,
            name:'EVENT_679',
            discount: 18
        }
    ]
    const {session} = req.params;
    const {discount} = req.body;
    const products = []
    const productList = await redis.hgetall('cart:'+session)
    if(_.isEmpty(productList)){
        return res.send('Giỏ hàng đang trống... ')
    }
    for (const product of Object.keys(productList)){
        products.push(JSON.parse(productList[product]))
    }
    const total = products.reduce((total,product) => total+=product.amount*product.price,0)
    const checkDiscount = _.get(_.find(discounts,item=>item.id === discount),'discount',0);
    const totalPrice = total - checkDiscount;
    const totalData = {
        products,
        totalBefore: total,
        discount: checkDiscount,
        total: totalPrice
    }
    return res.send(totalData)
})
app.post('/order/:session',async(req, res, next)=>{
    const discounts = [
        {
            id:1,
            name:'EVENT_678',
            discount: 20
        },
        {
            id:2,
            name:'EVENT_679',
            discount: 18
        }
    ]
    const {session} = req.params;
    const {discount} = req.body;
    const products = []
    const productList = await redis.hgetall('cart:'+session)
    if(_.isEmpty(productList)){
        return res.send('Giỏ hàng đang trống... ')
    }
    for (const product of Object.keys(productList)){
        products.push(JSON.parse(productList[product]))
    }
    const total = products.reduce((total,product) => total+=product.amount*product.price,0)
    const checkDiscount = _.get(_.find(discounts,item=>item.id === discount),'discount',0);
    const totalPrice = total - checkDiscount;
    const totalData = {
        orderId: uniqid(),
        orderDate: new Date(),
        products,
        totalBefore: total,
        discount: checkDiscount,
        total: totalPrice
    }
    let transactionId= uniqid();
    await asyncForEach(products,async(item)=>{
        await axios.post('http://localhost:3000/orderItem',{
            orderId: transactionId,
            productId: item.sku,
            quantity: item.amount,
            price: item.price,

        })
    })
    await axios.post('http://localhost:3000/order',{
        orderId: transactionId,
        userId: req.params.session,
        discount: checkDiscount,
        orderDate: new Date(),
        totalBefore: total,
        total: totalPrice
    })
    return res.send(totalData)
})
app.get('/order-detail/:order',async(req,res,next)=>{
    const {data} =  await axios.get('http://localhost:3000/orderItem')
    const findOnder =  _.filter(data,item=>item.orderId === req.params.order)
    // console.log('data',findOnder);
    return res.send(findOnder)
})
app.listen(3001,()=>{
    console.log('App listening on port 3001');
})